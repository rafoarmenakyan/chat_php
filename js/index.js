const sendMessageBtn = document.querySelector('.sendMessageBtn');
const nickNameInp = document.querySelector('.nickNameInp');
const messageInp = document.querySelector('.message');
const errorTxt = document.querySelector('.errorTxt');


const renderMessages = (element) => {
    $('.distributeMessages').append(`
        <div class="drawDiv">
            <p class="drawNickName">${element.nickName} :</p>
            <p class="drawMessage" data-id="${element.message_id}">${element.message}</p>
            <p class="createdAt">${element.createdAt}</p>
        </div>
    `);
}

const renderUsers = (element) => {
    $('.usersBlock').append(`
        <div class="drawUsersDiv">
            <p class="drawUsers" data-id="${element.id}">${element.nickName} :</p>
            <p class="isOnline">Online</p>
        </div>
    `);
};


const get = async (lastMessageDataId = 0, lastUserDataId = 0) => {
    let response = await $.ajax({
        url: 'chat.php',
        method: "POST",
        dataType:"json",
        data: {
            lastMessageDataId,
            lastUserDataId
        },
    });
    console.log(response);
    response.messages.forEach(message => {
        renderMessages(message);
    });

    $('.usersBlock').empty();
    response.users.forEach(user => {
        renderUsers(user);
    });
};


const send = async () => {
    const nickName = nickNameInp.value;
    const message = messageInp.value;

    let messages = document.querySelectorAll('.drawMessage');
    let lastMessageId = messages[messages.length-1]?.dataset.id || 0;
    
    let users = document.querySelectorAll('.drawUsers');
    let lastUserId = users[users.length-1]?.dataset.id || 0;

    if (!message.trim() || !nickName.trim()) {
        errorTxt.innerText = 'Declare Fields !!!'
        return
    } 
    errorTxt.innerText = '';

    await $.ajax({
        url: 'chat.php',
        method: "POST",
        dataType:"json",
        data: {
            nickName,
            message
        },
    });

    await get(lastMessageId, lastUserId);
    messageInp.value = '';
};


window.addEventListener('DOMContentLoaded', () => get());

setInterval(async () => {
    let messages = document.querySelectorAll('.drawMessage');
    let lastMessageDataId = messages[messages.length-1]?.dataset.id || 0;
    
    let users = document.querySelectorAll('.drawUsers');
    let lastUserDataId = users[users.length-1]?.dataset.id || 0;

    await get(lastMessageDataId, lastUserDataId);
}, 5000);


[messageInp, nickNameInp].forEach(inp => {
    inp.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
           event.preventDefault();
           send(); 
        }
    });
})

sendMessageBtn.addEventListener('click', () => send());