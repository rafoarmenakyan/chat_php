<?php
    require_once 'connect.php';


    if (isset($_POST['nickName']) && isset($_POST['message'])) {
		
		$nickName = $_POST['nickName'];
		$message = $_POST['message'];
		$now = date("Y-m-d H:i:s");

		$getNickName = mysqli_query($connection,"SELECT nickName FROM users WHERE nickName = '$nickName'");
		$checkNickName = mysqli_fetch_all($getNickName, MYSQLI_ASSOC);

		if (count($checkNickName) === 0) {
			mysqli_query($connection, "INSERT INTO users (nickName, lastActive) VALUES ('{$nickName}', '{$now}')");
			$userId = mysqli_query($connection, "SELECT LAST_INSERT_ID() FROM users");
		} else {
			mysqli_query($connection, "UPDATE users SET lastActive = '{$now}' WHERE users.nickName = '{$nickName}'");
		}
		
		$getUserId = mysqli_query($connection, "SELECT id FROM users WHERE nickName = '$nickName'");
		$userId =  mysqli_fetch_assoc($getUserId)['id'];

		mysqli_query($connection, "INSERT INTO messages (message, createdAt, userId) VALUES ('{$message}', '{$now}', {$userId})");
		
		echo json_encode([$getNickName]);
		
		exit;
	}	
	

	if (isset($_POST['lastMessageDataId']) && isset($_POST['lastUserDataId'])) {

		$lastMessageDataId = $_POST['lastMessageDataId'];
		$lastUserDataId = $_POST['lastUserDataId'];

		
		$joinTables =  mysqli_query($connection, "SELECT *, messages.id AS message_id, users.id AS user_id FROM messages INNER JOIN users ON users.id = messages.userId WHERE messages.id > {$lastMessageDataId}");
		$joinedArr =  mysqli_fetch_all($joinTables, MYSQLI_ASSOC);
		
		
		$getUsers = mysqli_query($connection, "SELECT * FROM `users` WHERE users.lastActive > DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
		$usersArr = mysqli_fetch_all($getUsers, MYSQLI_ASSOC);
		
		$arr = ['users'=>$usersArr, 'messages'=>$joinedArr];

		echo json_encode($arr);
		
		exit;
	}

    mysqli_close($connection);
	
	
?>