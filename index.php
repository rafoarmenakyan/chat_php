<?php
	require_once 'chat.php';
?> 



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<p class="errorTxt"></p>
	<div class="container">
		<div class="messages">
			<div class="distributeMessages">
				<h2 class="messagesTitle">Messages</h2>
			</div>
			<div class="sendMessages">
				<input type="text" class="nickNameInp" name="nickName" placeholder="Set NickName">
				<textarea name="message" cols="45" rows="2" placeholder="Write Message ..." class="message"></textarea>
				<button class="sendMessageBtn" name="sendMessage">Send Message</button>
			</div>
		</div>
		<div>
			<h2 class="usersTitle">Active Users</h2>
			<div class="usersBlock"></div>
		</div>
	</div>

	
	<script src="js/index.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>